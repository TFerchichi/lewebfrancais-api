<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiTest extends WebTestCase
{

    public function testGet200ForAllArticles()
    {
        $client = static::createClient();
        $client->followRedirects(true);
        $client->jsonRequest('GET', '/articles/');
        
        $content = $client->getResponse()->getContent();
        $data = json_decode($content, true);
        
        $dataExpected = '[
            {
              "id": 1,
              "nom": "Ski",
              "prix": 500
            },
            {
              "id": 2,
              "nom": "Batons",
              "prix": 50
            },
            {
              "id": 3,
              "nom": "Casque",
              "prix": 100
            }
        ]';
        
        // Vérification du contenu JSON retourné
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals(json_decode($dataExpected), $data);
    }

    public function testGet200ForShop1InfoWithAdmin1(){
        
        $client = static::createClient();
        $client->followRedirects(true);
        $client->request('GET', '/boutique/1/infos', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'user' => [
                'login' => 'admin1',
                'password' => 'admin1'
            ]
        ]));

        $content = $client->getResponse()->getContent();
        $dataExpected = '{
            "id": 1,
            "stock": [
              {
                "article": {
                  "id": 1,
                  "nom": "Ski",
                  "prix": 50
                },
                "tarifLocationJour": 25,
                "stock": 10
              },
              {
                "article": {
                  "id": 3,
                  "nom": "Casque",
                  "prix": 100
                },
                "tarifLocationJour": 5,
                "stock": 20
              }
            ],
            "personnel": [
              {
                "id": 1,
                "displayName": "Admin 1"
              },
              {
                "id": 2,
                "displayName": "Vendeur 1"
              }
            ]
          }';
        
          $data = json_decode($content, true);

        // Vérification du contenu JSON retourné egale à $dataExpected
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals(json_decode($dataExpected), $data);

    }

    public function testGet200ForShop1InfoWithVendor1(){
        
        $client = static::createClient();
        $client->followRedirects(true);
        $client->request('GET', '/boutique/1/infos', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'user' => [
                'login' => 'admin1',
                'password' => 'admin1'
            ]
        ]));

        $content = $client->getResponse()->getContent();
        $dataExpected = '{
            "id": 1,
            "stock": [
              {
                "article": {
                  "id": 1,
                  "nom": "Ski",
                  "prix": 50
                },
                "tarifLocationJour": 25,
                "stock": 10
              },
              {
                "article": {
                  "id": 3,
                  "nom": "Casque",
                  "prix": 100
                },
                "tarifLocationJour": 5,
                "stock": 20
              }
            ],
            "personnel": [
              {
                "id": 1,
                "displayName": "Admin 1"
              },
              {
                "id": 2,
                "displayName": "Vendeur 1"
              }
            ]
          }';
        
          $data = json_decode($content, true);

        // Vérification du contenu JSON retourné egale à $dataExpected
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals(json_decode($dataExpected), $data);
    }

    public function testGet403ForShop1InfoWithAdmin2(){
        
        $client = static::createClient();
        $client->followRedirects(true);
        $client->request('GET', '/boutique/1/infos', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'user' => [
                'login' => 'admin1',
                'password' => 'admin1'
            ]
        ]));

        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $data = json_decode($content, true);

        // Vérification du contenu JSON retourné
        $this->assertIsArray($data);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(403, $data['code']);
        $this->assertArrayHasKey('error', $data);
        $this->assertEquals('Accès interdit', $data['error']);
    }

    public function testGet403ForShop1InfosWithVendor2()
    {
        $client = static::createClient();
        $client->followRedirects(true);
        $client->request('GET', '/boutique/1/infos', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'user' => [
                'login' => 'admin1',
                'password' => 'admin1'
            ]
        ]));

        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $data = json_decode($content, true);

        // Vérification du contenu JSON retourné
        $this->assertIsArray($data);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(403, $data['code']);
        $this->assertArrayHasKey('error', $data);
        $this->assertEquals('Accès interdit', $data['error']);
    }

    public function testGet404ForShop1Product2WithAdmin1()
    {
        $client = static::createClient();
        $client->followRedirects(true);
        $client->request('GET', '/boutique/1/articles/2', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'user' => [
                'login' => 'admin1',
                'password' => 'admin1'
            ]
        ]));

        $this->assertEquals(404, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $data = json_decode($content, true);

        // Vérification du contenu JSON retourné
        $this->assertIsArray($data);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(404, $data['code']);
        $this->assertArrayHasKey('error', $data);
        $this->assertEquals('Article introuvable', $data['error']);
    }

    public function testGet404ForShop1Product2WithVendor1()
    {
        $client = static::createClient();
        $client->followRedirects(true);
        $client->request('GET', '/boutique/1/articles/2', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'user' => [
                'login' => 'admin1',
                'password' => 'admin1'
            ]
        ]));

        $this->assertEquals(404, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $data = json_decode($content, true);

        // Vérification du contenu JSON retourné
        $this->assertIsArray($data);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(404, $data['code']);
        $this->assertArrayHasKey('error', $data);
        $this->assertEquals('Article introuvable', $data['error']);
    }

}

<?php

namespace App\Service;

use App\Entity\Shop;
use App\Entity\User;
use App\Repository\ShopRepository;
use App\Repository\UserShopRoleRepository;

class ShopHelper
{

    /**
     * @var ShopRepository
     */
    private ShopRepository $shopRepository;

    /**
     * @var UserShopRoleRepository
     */
    private UserShopRoleRepository $userShopRoleRepository;

    public function __construct(ShopRepository $shopRepository, UserShopRoleRepository $userShopRoleRepository)
    {
        $this->shopRepository = $shopRepository;
        $this->userShopRoleRepository = $userShopRoleRepository;
    }

    public function getShop(int $id) : ?Shop
    {
        $shop = $this->shopRepository->find($id);

        return $shop;
    }
    
    /**
     * @return Product[]
     */
    public function getShopProducts(Shop $shop) : array
    {
        $products = $shop->getShopProducts()->getValues();
        
        return $products;
    }

    /**
     * @return User[]
     */
    public function getShopEmployees(Shop $shop) : array
    {
        $employees = [];
        $assignments = $this->userShopRoleRepository->findAll(['shop' => $shop]);
        $employees = array_map(function($assignment) {
           return $assignment->getUser();
        }, $assignments);

        return $employees;
    }

    public function isAssignedToShop(User $user, int $shopId) : bool
    {
        $assignment = $this->userShopRoleRepository->findOneBy(['user' => $user, 'shop' => $shopId]);

        return $assignment ? true : false;
    }

    public function getShopInfos(int $shopId) : array
    {
        $shop = $this->shopRepository->find($shopId);

        $shopProducts = array_map(function($product) {
            return [
                'article' => [
                    'id' => $product->getProduct()->getId(),
                    'nom' => $product->getProduct()->getName(),
                    'prix' => $product->getProduct()->getPrice()
                ]
            ];
        }, $this->getShopProducts($shop));

        $shopEmployees = array_map(function($employee) {
            return [
                'id' => $employee->getId(),
                'displayName' => $employee->getDisplayName()
            ];
        }, $this->getShopEmployees($shop));

        $shopInfo = [
            'id' => $shop->getId(),
            'stock' => $shopProducts,
            'personnel' => $shopEmployees
        ];

        return $shopInfo;
    }
}

<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;

class UserHelper
{

    /**
     * @var User
     */
    private User $user;

    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function checkUser($user): ?User
    {

        $user = $this->userRepository->findOneBy(
            [
                'login' => $user->login,
                'password' => $user->password
            ]
        );

        return $user;
    }
}

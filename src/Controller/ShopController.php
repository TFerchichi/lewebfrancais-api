<?php

namespace App\Controller;

use App\Repository\ShopRepository;
use App\Service\ShopHelper;
use App\Service\UserHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/boutique', name: 'app_shop')]
class ShopController extends AbstractController
{

    /**
     * @var ShopRepository
     */
    private ShopRepository $shopRepository;

    /**
     * @var UserHelper
     */
    private UserHelper $userHelper;

    /**
     * @var ShopHelper
     */
    private ShopHelper $shopHelper;

    public function __construct(ShopRepository $shopRepository, UserHelper $userHelper, ShopHelper $shopHelper)
    {
        $this->shopRepository = $shopRepository;
        $this->userHelper = $userHelper;
        $this->shopHelper = $shopHelper;
    }

    #[Route('/{id}/infos', name: '_index', methods: ['GET'])]
    public function getShopInfos(Request $request) : JsonResponse
    {
        $content = json_decode($request->getContent());
        
        $user = $this->userHelper->checkUser($content->user);
        $shopId = $request->get('id');
        
        if ($user) {
            if ($this->shopHelper->isAssignedToShop($user, $shopId)) {
                $shopInfo = $this->shopHelper->getShopInfos($shopId);
                return new JsonResponse($shopInfo);
            }
            else{
                return new JsonResponse(['message' => 'Accès interdit'], 403);
            }
        }
        else{
            return new JsonResponse(['message' => 'Non Authentifié'], 401);
        }
    }
}

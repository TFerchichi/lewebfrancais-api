<?php

namespace App\Entity;

use App\Repository\ProductShopRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductShopRepository::class)]
class ProductShop
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToMany(targetEntity: Shop::class, inversedBy: 'productShops')]
    private Collection $product;

    public function __construct()
    {
        $this->product = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Shop>
     */
    public function getProduct(): Collection
    {
        return $this->product;
    }

    public function addProduct(Shop $product): static
    {
        if (!$this->product->contains($product)) {
            $this->product->add($product);
        }

        return $this;
    }

    public function removeProduct(Shop $product): static
    {
        $this->product->removeElement($product);

        return $this;
    }
}

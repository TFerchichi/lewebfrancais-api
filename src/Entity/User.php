<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UserRepository::class)]
class User
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $login = null;

    #[ORM\Column(length: 255)]
    private ?string $password = null;

    #[ORM\Column(length: 150)]
    private ?string $displayName = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: UserShopRole::class)]
    private Collection $userShopRoles;

    public function __construct()
    {
        $this->userShopRoles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): static
    {
        $this->login = $login;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    public function setDisplayName(string $displayName): static
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * @return Collection<int, UserShopRole>
     */
    public function getUserShopRoles(): Collection
    {
        return $this->userShopRoles;
    }

    public function addUserShopRole(UserShopRole $userShopRole): static
    {
        if (!$this->userShopRoles->contains($userShopRole)) {
            $this->userShopRoles->add($userShopRole);
            $userShopRole->setUser($this);
        }

        return $this;
    }

    public function removeUserShopRole(UserShopRole $userShopRole): static
    {
        if ($this->userShopRoles->removeElement($userShopRole)) {
            // set the owning side to null (unless already changed)
            if ($userShopRole->getUser() === $this) {
                $userShopRole->setUser(null);
            }
        }

        return $this;
    }
}

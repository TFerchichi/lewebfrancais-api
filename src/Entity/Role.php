<?php

namespace App\Entity;

use App\Repository\RoleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RoleRepository::class)]
class Role
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 150)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'role', targetEntity: UserShopRole::class)]
    private Collection $userShopRoles;

    public function __construct()
    {
        $this->userShopRoles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, UserShopRole>
     */
    public function getUserShopRoles(): Collection
    {
        return $this->userShopRoles;
    }

    public function addUserShopRole(UserShopRole $userShopRole): static
    {
        if (!$this->userShopRoles->contains($userShopRole)) {
            $this->userShopRoles->add($userShopRole);
            $userShopRole->setRole($this);
        }

        return $this;
    }

    public function removeUserShopRole(UserShopRole $userShopRole): static
    {
        if ($this->userShopRoles->removeElement($userShopRole)) {
            // set the owning side to null (unless already changed)
            if ($userShopRole->getRole() === $this) {
                $userShopRole->setRole(null);
            }
        }

        return $this;
    }
}

<?php

namespace App\Repository;

use App\Entity\UserShopRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UserShopRole>
 *
 * @method UserShopRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserShopRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserShopRole[]    findAll()
 * @method UserShopRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserShopRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserShopRole::class);
    }

//    /**
//     * @return UserShopRole[] Returns an array of UserShopRole objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('u.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?UserShopRole
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

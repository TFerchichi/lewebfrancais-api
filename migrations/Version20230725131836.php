<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230725131836 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_shop_role (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, shop_id INT DEFAULT NULL, role_id INT NOT NULL, INDEX IDX_967BE1F5A76ED395 (user_id), INDEX IDX_967BE1F54D16C4DD (shop_id), INDEX IDX_967BE1F5D60322AC (role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_shop_role ADD CONSTRAINT FK_967BE1F5A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_shop_role ADD CONSTRAINT FK_967BE1F54D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id)');
        $this->addSql('ALTER TABLE user_shop_role ADD CONSTRAINT FK_967BE1F5D60322AC FOREIGN KEY (role_id) REFERENCES role (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_shop_role DROP FOREIGN KEY FK_967BE1F5A76ED395');
        $this->addSql('ALTER TABLE user_shop_role DROP FOREIGN KEY FK_967BE1F54D16C4DD');
        $this->addSql('ALTER TABLE user_shop_role DROP FOREIGN KEY FK_967BE1F5D60322AC');
        $this->addSql('DROP TABLE user_shop_role');
    }
}
